<?php

namespace Customize\Twig\Extension;

use Customize\Repository\LatestProductRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class LatestProductExtension extends AbstractExtension {
  protected $repository;

  public function __construct(LatestProductRepository $repository) {
    $this->repository = $repository;
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_latest_products', function () {
        $products = $this->repository->getLatestProductList();

        return $products;
      }, ['is_safe' => ['all']]),
    ];
  }
}

