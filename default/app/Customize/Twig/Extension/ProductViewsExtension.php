<?php

namespace Customize\Twig\Extension;

use Customize\Repository\ProductViewsRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ProductViewsExtension extends AbstractExtension {
  protected $repository;

  public function __construct(ProductViewsRepository $repository) {
    $this->repository = $repository;
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_popular_products', function () {
        $products = $this->repository->getMostViewedProducts();

        return $products;
      }, ['is_safe' => ['all']]),
    ];
  }
}

