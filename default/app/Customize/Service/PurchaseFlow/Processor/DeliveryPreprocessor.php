<?php

namespace Customize\Service\PurchaseFlow\Processor;

use Eccube\Annotation\ShoppingFlow;
use Eccube\Entity\ItemHolderInterface;
use Eccube\Entity\Order;
use Eccube\Service\PurchaseFlow\ItemHolderPreprocessor;
use Eccube\Service\PurchaseFlow\PurchaseContext;
use Eccube\Service\PurchaseFlow\Processor\DeliveryFeePreprocessor;

/**
 *
 * @ShoppingFlow()
 *
 * Class DeliveryPreprocessor
 * @package Customize\Service\PurchaseFlow\Processor
 */
class DeliveryPreprocessor implements ItemHolderPreprocessor {
    public function process(ItemHolderInterface $itemHolder, PurchaseContext $context) {
        if ($itemHolder instanceof Order) {
            $Order = $itemHolder;
            foreach ($Order->getShippings() as $Shipping) {
                $isFree = false;
                $regular = 0;
                foreach ($Shipping->getProductOrderItems() as $Item) {
                    $regular += $Item->getPriceIncTax() * $Item->getQuantity();
                }

                foreach ($Shipping->getOrderItems() as $Item) {
                    if ($Item->getProcessorName() == DeliveryFeePreprocessor::class) {
                        if ($regular >= 30000) {
                            $Item->setQuantity(0);
                        }
                    }
                }
            }
        }
    }
}
