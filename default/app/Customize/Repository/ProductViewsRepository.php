<?php

namespace Customize\Repository;

use Eccube\Application;
use Eccube\Repository\AbstractRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Customize\Entity\ProductViews;
use Symfony\Component\Form\FormFactory;
use Eccube\Form\Type\AddCartType;
use Eccube\Repository\ProductRepository;

class ProductViewsRepository extends AbstractRepository {
  protected $formFactory;
  protected $productRepository;

  public function __construct(ManagerRegistry $registry, FormFactory $formFactory, ProductRepository $productRepository) {
    parent::__construct($registry, ProductViews::class);
    $this->formFactory = $formFactory;
    $this->productRepository = $productRepository;
  }

  public function getMostViewedProducts() {
    $qb = $this->_em->createQueryBuilder();
    $results = $qb->select(['p', 'COALESCE(pv.views, 0) AS views'])
                  ->from('Eccube\Entity\Product', 'p')
                  ->leftJoin('Customize\Entity\ProductViews', 'pv', 'WITH', 'pv.Product = p')
                  ->andWhere('p.Status = 1')
                  ->orderBy('views', 'DESC')
                  ->addOrderBy('p.create_date', 'DESC')
                  ->addOrderBy('p.id', 'DESC')
                  ->setMaxResults(20)
                  ->getQuery()
                  ->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])
                  ->getResult();

    $ids = [];
    foreach ($results as $Product) {
      $ids[] = $Product[0]->getId();
    }
    $ProductsAndClassCategories = $this->productRepository->findProductsWithSortedClassCategories($ids, 'p.id');

    $forms = [];
    foreach ($results as $Product) {
      /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
      $builder = $this->formFactory->createNamedBuilder(
        '',
        AddCartType::class,
        null,
        [
          'product' => $ProductsAndClassCategories[$Product[0]->getId()],
          'allow_extra_fields' => true,
        ]
      );
      $addCartForm = $builder->getForm();

      $forms[$Product[0]->getId()] = $addCartForm->createView();
    }

    return array(
      'products' => $results,
      'forms' => $forms,
    );
  }

  public function findByProduct($product) {
    $views = $this->findOneBy(['Product' => $product]);
    if (!$views) {
      $views = new ProductViews;
      $views->setViews(0);
      $views->setProduct($product);
      $this->_em->persist($views);
      $this->_em->flush();
    }
    return $views;
  }

  public function incrementViews($product) {
    $this->findByProduct($product); // Ensure product view row is created
    return $this->_em->createQuery(
      'UPDATE Customize\Entity\ProductViews pv SET pv.views = pv.views + 1 WHERE pv.Product = :product'
    )->setParameter('product', $product)->execute();
  }
}
