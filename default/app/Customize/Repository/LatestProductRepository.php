<?php

namespace Customize\Repository;

use Eccube\Repository\AbstractRepository;
use Eccube\Entity\Product;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactory;
use Eccube\Form\Type\AddCartType;
use Eccube\Repository\ProductRepository;

class LatestProductRepository extends AbstractRepository {
  protected $formFactory;
  protected $productRepository;

  public function __construct(RegistryInterface $registry, FormFactory $formFactory, ProductRepository $productRepository) {
    parent::__construct($registry, Product::class);
    $this->formFactory = $formFactory;
    $this->productRepository = $productRepository;
  }

  public function getLatestProductList() {
    $qb = $this->createQueryBuilder('p')
               ->andWhere('p.Status = 1')
               ->orderBy('p.create_date', 'DESC')
               ->addOrderBy('p.id', 'DESC')
               ->setMaxResults(20);

    $products = $qb->getQuery()
                   ->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])
                   ->getResult();

    $ids = [];
    foreach ($products as $Product) {
      $ids[] = $Product->getId();
    }
    $ProductsAndClassCategories = $this->productRepository->findProductsWithSortedClassCategories($ids, 'p.id');

    $forms = [];
    foreach ($products as $Product) {
      /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
      $builder = $this->formFactory->createNamedBuilder(
        '',
        AddCartType::class,
        null,
        [
          'product' => $ProductsAndClassCategories[$Product->getId()],
          'allow_extra_fields' => true,
        ]
      );
      $addCartForm = $builder->getForm();

      $forms[$Product->getId()] = $addCartForm->createView();
    }

    return array(
      'products' => $products,
      'forms' => $forms,
    );
  }
}
