<?php

namespace Customize\Repository;

use Eccube\Repository\QueryKey;
use Eccube\Doctrine\Query\QueryCustomizer;
use Doctrine\ORM\QueryBuilder;

class ProductListCustomizer implements QueryCustomizer {
  public function getQueryKey() {
    return QueryKey::PRODUCT_SEARCH;
  }

  public function customize(QueryBuilder $builder, $params, $queryKey) {
    if ($params['has_stock']) {
      if (!in_array('pc', $builder->getAllAliases())) {
        $builder->innerJoin('p.ProductClasses', 'pc');
      }
      $builder->andWhere('pc.stock_unlimited = true OR pc.stock > 0');
    }
    return $builder;
  }
}
