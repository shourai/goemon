<?php

namespace Customize\Controller;

use Eccube\Entity\ProductCategory;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Eccube\Util\CacheUtil;
use Eccube\Entity\Product;
use Eccube\Entity\ProductClass;
use Eccube\Repository\Master\ProductStatusRepository;
use Eccube\Entity\BaseInfo;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Repository\TaxRuleRepository;
use Eccube\Repository\TagRepository;
use Eccube\Repository\CategoryRepository;
use Eccube\Repository\DeliveryDurationRepository;
use Eccube\Entity\Master\ProductStatus;
use Eccube\Entity\ProductStock;
use Eccube\Form\Type\Admin\ProductType;
use Eccube\Event\EventArgs;
use Eccube\Event\EccubeEvents;
use Eccube\Form\Type\Admin\SearchProductType;

class AdminProductController extends AbstractController {
  protected $productStatusRepository;
  protected $BaseInfo;
  protected $taxRuleRepository;
  protected $tagRepository;
  protected $categoryRepository;
  protected $deliveryDurationRepository;

  public function __construct(ProductStatusRepository $productStatusRepository, BaseInfoRepository $baseInfoRepository, TaxRuleRepository $taxRuleRepository, TagRepository $tagRepository, CategoryRepository $categoryRepository, DeliveryDurationRepository $deliveryDurationRepository) {
    $this->productStatusRepository = $productStatusRepository;
    $this->BaseInfo = $baseInfoRepository->get();
    $this->taxRuleRepository = $taxRuleRepository;
    $this->tagRepository = $tagRepository;
    $this->categoryRepository = $categoryRepository;
    $this->deliveryDurationRepository = $deliveryDurationRepository;
  }

    /**
     * ProductCategory作成
     *
     * @param \Eccube\Entity\Product $Product
     * @param \Eccube\Entity\Category $Category
     * @param integer $count
     *
     * @return \Eccube\Entity\ProductCategory
     */
    private function createProductCategory($Product, $Category, $count)
    {
        $ProductCategory = new ProductCategory();
        $ProductCategory->setProduct($Product);
        $ProductCategory->setProductId($Product->getId());
        $ProductCategory->setCategory($Category);
        $ProductCategory->setCategoryId($Category->getId());

        return $ProductCategory;
    }

  /**
   * @Route("/%eccube_admin_route%/product/product/new", name="admin_product_product_new")
   * @Template("@admin/Product/product.twig")
   */
  public function overrideNew(Request $request, RouterInterface $router, CacheUtil $cacheUtil) {
    $Product = new Product();
    $ProductClass = new ProductClass();
    $ProductStatus = $this->productStatusRepository->find(ProductStatus::DISPLAY_SHOW);
    $DeliveryDuration = $this->deliveryDurationRepository->find(2);
    $Product
      ->addProductClass($ProductClass)
      ->setStatus($ProductStatus);
    $ProductClass
      ->setVisible(true)
      ->setStock(1)
      ->setStockUnlimited(false)
      ->setSaleLimit(1)
      ->setDeliveryDuration($DeliveryDuration)
      ->setProduct($Product);
    $ProductStock = new ProductStock();
    $ProductClass->setProductStock($ProductStock);
    $ProductStock->setProductClass($ProductClass);

    $builder = $this->formFactory
                    ->createBuilder(ProductType::class, $Product);

    $event = new EventArgs(
      [
        'builder' => $builder,
        'Product' => $Product,
      ],
      $request
    );
    $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_PRODUCT_EDIT_INITIALIZE, $event);

    $form = $builder->getForm();

    $ProductClass->setStockUnlimited($ProductClass->isStockUnlimited());
    $form['class']->setData($ProductClass);

    // ファイルの登録
    $images = [];
    $ProductImages = $Product->getProductImage();
    foreach ($ProductImages as $ProductImage) {
      $images[] = $ProductImage->getFileName();
    }
    $form['images']->setData($images);

    $categories = [];
    $ProductCategories = $Product->getProductCategories();
    foreach ($ProductCategories as $ProductCategory) {
      /* @var $ProductCategory \Eccube\Entity\ProductCategory */
      $categories[] = $ProductCategory->getCategory();
    }
    $form['Category']->setData($categories);

    $Tags = $Product->getTags();
    $form['Tag']->setData($Tags);

    if ('POST' === $request->getMethod()) {
      $form->handleRequest($request);
      if ($form->isValid()) {
        $Product = $form->getData();

        $ProductClass = $form['class']->getData();

        // 個別消費税
        if ($this->BaseInfo->isOptionProductTaxRule()) {
          if ($ProductClass->getTaxRate() !== null) {
            if ($ProductClass->getTaxRule()) {
              $ProductClass->getTaxRule()->setTaxRate($ProductClass->getTaxRate());
            } else {
              $taxrule = $this->taxRuleRepository->newTaxRule();
              $taxrule->setTaxRate($ProductClass->getTaxRate());
              $taxrule->setApplyDate(new \DateTime());
              $taxrule->setProduct($Product);
              $taxrule->setProductClass($ProductClass);
              $ProductClass->setTaxRule($taxrule);
            }

            $ProductClass->getTaxRule()->setTaxRate($ProductClass->getTaxRate());
          } else {
            if ($ProductClass->getTaxRule()) {
              $this->taxRuleRepository->delete($ProductClass->getTaxRule());
              $ProductClass->setTaxRule(null);
            }
          }
        }
        $this->entityManager->persist($ProductClass);

        // 在庫情報を作成
        if (!$ProductClass->isStockUnlimited()) {
          $ProductStock->setStock($ProductClass->getStock());
        } else {
          // 在庫無制限時はnullを設定
          $ProductStock->setStock(null);
        }
        $this->entityManager->persist($ProductStock);

        // カテゴリの登録
        // 一度クリア
        /* @var $Product \Eccube\Entity\Product */
        foreach ($Product->getProductCategories() as $ProductCategory) {
          $Product->removeProductCategory($ProductCategory);
          $this->entityManager->remove($ProductCategory);
        }
        $this->entityManager->persist($Product);
        $this->entityManager->flush();

        $count = 1;
        $Categories = $form->get('Category')->getData();
        $categoriesIdList = [];
        foreach ($Categories as $Category) {
          foreach ($Category->getPath() as $ParentCategory) {
            if (!isset($categoriesIdList[$ParentCategory->getId()])) {
              $ProductCategory = $this->createProductCategory($Product, $ParentCategory, $count);
              $this->entityManager->persist($ProductCategory);
              $count++;
              /* @var $Product \Eccube\Entity\Product */
              $Product->addProductCategory($ProductCategory);
              $categoriesIdList[$ParentCategory->getId()] = true;
            }
          }
          if (!isset($categoriesIdList[$Category->getId()])) {
            $ProductCategory = $this->createProductCategory($Product, $Category, $count);
            $this->entityManager->persist($ProductCategory);
            $count++;
            /* @var $Product \Eccube\Entity\Product */
            $Product->addProductCategory($ProductCategory);
            $categoriesIdList[$ParentCategory->getId()] = true;
          }
        }

        // 画像の登録
        $add_images = $form->get('add_images')->getData();
        foreach ($add_images as $add_image) {
          $ProductImage = new \Eccube\Entity\ProductImage();
          $ProductImage
            ->setFileName($add_image)
            ->setProduct($Product)
            ->setSortNo(1);
          $Product->addProductImage($ProductImage);
          $this->entityManager->persist($ProductImage);

          // 移動
          $file = new File($this->eccubeConfig['eccube_temp_image_dir'].'/'.$add_image);
          $file->move($this->eccubeConfig['eccube_save_image_dir']);
        }

        // 画像の削除
        $delete_images = $form->get('delete_images')->getData();
        foreach ($delete_images as $delete_image) {
          $ProductImage = $this->productImageRepository
                               ->findOneBy(['file_name' => $delete_image]);

          // 追加してすぐに削除した画像は、Entityに追加されない
          if ($ProductImage instanceof ProductImage) {
            $Product->removeProductImage($ProductImage);
            $this->entityManager->remove($ProductImage);
          }
          $this->entityManager->persist($Product);

          // 削除
          $fs = new Filesystem();
          $fs->remove($this->eccubeConfig['eccube_save_image_dir'].'/'.$delete_image);
        }
        $this->entityManager->persist($Product);
        $this->entityManager->flush();

        $sortNos = $request->get('sort_no_images');
        if ($sortNos) {
          foreach ($sortNos as $sortNo) {
            list($filename, $sortNo_val) = explode('//', $sortNo);
            $ProductImage = $this->productImageRepository
                                 ->findOneBy([
                                   'file_name' => $filename,
                                   'Product' => $Product,
                                 ]);
            $ProductImage->setSortNo($sortNo_val);
            $this->entityManager->persist($ProductImage);
          }
        }
        $this->entityManager->flush();

        // 商品タグの登録
        // 商品タグを一度クリア
        $ProductTags = $Product->getProductTag();
        foreach ($ProductTags as $ProductTag) {
          $Product->removeProductTag($ProductTag);
          $this->entityManager->remove($ProductTag);
        }

        // 商品タグの登録
        $Tags = $form->get('Tag')->getData();
        foreach ($Tags as $Tag) {
          $ProductTag = new ProductTag();
          $ProductTag
            ->setProduct($Product)
            ->setTag($Tag);
          $Product->addProductTag($ProductTag);
          $this->entityManager->persist($ProductTag);
        }

        $Product->setUpdateDate(new \DateTime());
        $this->entityManager->flush();

        $event = new EventArgs(
          [
            'form' => $form,
            'Product' => $Product,
          ],
          $request
        );
        $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_PRODUCT_EDIT_COMPLETE, $event);

        $this->addSuccess('admin.common.save_complete', 'admin');

        if ($returnLink = $form->get('return_link')->getData()) {
          try {
            // $returnLinkはpathの形式で渡される. pathが存在するかをルータでチェックする.
            $pattern = '/^'.preg_quote($request->getBasePath(), '/').'/';
            $returnLink = preg_replace($pattern, '', $returnLink);
            $result = $router->match($returnLink);
            // パラメータのみ抽出
            $params = array_filter($result, function ($key) {
              return 0 !== \strpos($key, '_');
            }, ARRAY_FILTER_USE_KEY);

            // pathからurlを再構築してリダイレクト.
            return $this->redirectToRoute($result['_route'], $params);
          } catch (\Exception $e) {
            // マッチしない場合はログ出力してスキップ.
            log_warning('URLの形式が不正です。');
          }
        }

        $cacheUtil->clearDoctrineCache();

        return $this->redirectToRoute('admin_product_product_edit', ['id' => $Product->getId()]);
      }
    }

    // 検索結果の保持
    $builder = $this->formFactory
                    ->createBuilder(SearchProductType::class);

    $event = new EventArgs(
      [
        'builder' => $builder,
        'Product' => $Product,
      ],
      $request
    );
    $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_PRODUCT_EDIT_SEARCH, $event);

    $searchForm = $builder->getForm();

    if ('POST' === $request->getMethod()) {
      $searchForm->handleRequest($request);
    }

    // Get Tags
    $TagsList = $this->tagRepository->getList();

    // ツリー表示のため、ルートからのカテゴリを取得
    $TopCategories = $this->categoryRepository->getList(null);
    $ChoicedCategoryIds = array_map(function ($Category) {
      return $Category->getId();
    }, $form->get('Category')->getData());

    return [
      'Product' => $Product,
      'Tags' => $Tags,
      'TagsList' => $TagsList,
      'form' => $form->createView(),
      'searchForm' => $searchForm->createView(),
      'has_class' => false,
      'id' => null,
      'TopCategories' => $TopCategories,
      'ChoicedCategoryIds' => $ChoicedCategoryIds,
    ];
  }
}
