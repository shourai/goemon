<?php

namespace Customize\Form\Extension;

use Eccube\Form\Type\SearchProductType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SearchProductStockExtension extends AbstractTypeExtension {
  public function getExtendedType() {
    return SearchProductType::class;
  }

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('has_stock', CheckboxType::class, [
      'label' => '在庫あり',
      'value' => '1',
      'required' => false,
    ]);
  }
}
