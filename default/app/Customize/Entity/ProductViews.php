<?php

namespace Customize\Entity;

use Eccube\Entity\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RelatedProduct.
 *
 * @ORM\Table(name="ctm_product_views")
 * @ORM\Entity(repositoryClass="Customize\Repository\ProductViewsRepository")
 */
class ProductViews {
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", options={"unsigned":true})
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

    /**
   * @var Product
   *
   * @ORM\OneToOne(targetEntity="Eccube\Entity\Product")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
   * })
   */
  private $Product;

  /**
   * @var int
   *
   * @ORM\Column(name="views", type="integer", options={"unsigned":true})
   */
  private $views;

  public function getId() {
    return $this->id;
  }

  public function getViews() {
    return $this->views;
  }

  public function setViews($views = 0) {
    $this->views = $views;
    return $this;
  }

  public function getProduct() {
    return $this->Product;
  }

  public function setProduct(Product $Product) {
    $this->Product = $Product;
    return $this;
  }
}
