<?php

namespace Customize\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Customize\Repository\ProductViewsRepository;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;

class ProductViewsEvent implements EventSubscriberInterface {
  protected $repo;

  public function __construct(ProductViewsRepository $repo) {
    $this->repo = $repo;
  }

  public static function getSubscribedEvents() {
    return [
      EccubeEvents::FRONT_PRODUCT_DETAIL_INITIALIZE => 'onProductDetail',
    ];
  }

  public function onProductDetail(EventArgs $event) {
    $Product = $event->getArgument('Product');
    $this->repo->incrementViews($Product);
  }
}
