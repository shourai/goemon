<?php

namespace Plugin\GmoEpsilon4\Entity;

use Eccube\Annotation\EntityExtension;
use Doctrine\ORM\Mapping as ORM;

/**
 * @EntityExtension("Eccube\Entity\Order")
 */
trait OrderTrait
{

    /**
     * @var string
     * 
     * @ORM\Column(name="trans_code", type="string", length=255, nullable=true)
     */
    private $trans_code;

    /**
     * {@inheritdoc}
     */
    public function setTransCode($transCode)
    {
        $this->trans_code = $transCode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransCode()
    {
        return $this->trans_code;
    }

}
