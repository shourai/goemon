<?php

namespace Plugin\GmoEpsilon4\Service\Method;

use Plugin\GmoEpsilon4\Service\Method\GmoEpsilon;

class Reg_Credit extends GmoEpsilon
{

    public function init()
    {
        $this->st_code = $this->eccubeConfig['gmo_epsilon']['st_code']['reg_credit'];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $dispatcher = $this->accessBlockProcess();

        // エラーがセットされていなければ正常処理を行う
        if (!$dispatcher->getResponse()) {
            $dispatcher = parent::apply();
        }

        return $dispatcher;
    }

    /**
     * チェックするレスポンスパラメータのキーを取得
     *
     * @return array
     */
    function getCheckParameter()
    {
        return ['trans_code', 'user_id', 'result', 'order_number'];
    }

    /**
     * リクエストパラメータを設定
     *
     * @param \Eccube\Entity\Order $Order
     * @return array
     */
    function setParameter($Order)
    {
        $arrParameter = parent::setParameter($Order);

        // 非会員の場合、空文字に置き換え ※エラーとする
        $arrParameter['user_id'] = $arrParameter['user_id'] == 'non_customer' ? '' : $arrParameter['user_id'];

        // 処理区分(登録済み課金)
        $arrParameter['process_code'] = "2";

        return $arrParameter;
    }
}
