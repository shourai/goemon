<?php

namespace Plugin\GmoEpsilon4\Service\Method;

use Plugin\GmoEpsilon4\Service\Method\GmoEpsilon;

class Credit extends GmoEpsilon
{

    public function init() {
        $this->st_code = $this->eccubeConfig['gmo_epsilon']['st_code']['credit'];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $dispatcher = $this->accessBlockProcess();

        // エラーがセットされていなければ正常処理を行う
        if (!$dispatcher->getResponse()) {
            $dispatcher = parent::apply();
        }

        return $dispatcher;
    }

    /**
     * チェックするレスポンスパラメータのキーを取得
     *
     * @return array
     */
    function getCheckParameter()
    {
        return ['trans_code', 'user_id', 'result', 'order_number'];
    }


}

 ?>
