<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\Tools\SchemaTool;

use Eccube\Application;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421105528 extends AbstractMigration {
  protected $entities = [
    'Customize\Entity\ProductViews',
  ];

  public function up(Schema $schema) : void {
    $app = Application::getInstance();
    $em = $app['orm.em'];
    $classes = array();
    foreach ($this->entities as $entity) {
      $classes[] = $em->getMetadataFactory()->getMetadataFor($entity);
    }
    $tool = new SchemaTool($em);
    $tool->createSchema($classes);

    $this->addSql('INSERT INTO dtb_block (device_type_id, block_name, file_name, use_controller, deletable, create_date, update_date, discriminator_type) SELECT id, \'人気商品\', \'popular_products\', 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, \'block\' FROM mtb_device_type WHERE name = \'PC\'');
  }

  public function down(Schema $schema) : void {
    $app = Application::getInstance();
    $em = $app['orm.em'];
    $classes = array();
    foreach ($this->entities as $entity) {
      $classes[] = $em->getMetadataFactory()->getMetadataFor($entity);
    }
    $tool = new SchemaTool($em);
    $tool->dropSchema($classes);

    $this->addSql('DELETE FROM dtb_block WHERE file_name = \'popular_products\'');
  }
}
