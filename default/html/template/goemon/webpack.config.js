
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

module.exports = {
    //mode: "development" or "production",
    mode: 'development',
    entry: {
        bundle: './assets/js/src/mdc_index.js',
        main: './assets/js/src/main_index.js'
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: ['lodash'],
                }
            }]
        }]
    },
    plugins: [
        new LodashModuleReplacementPlugin
    ],
    optimization: {
        minimizer: [new UglifyJsPlugin({
            extractComments: true,
        })]
    },
  	externals: {
        jquery: 'jQuery'
    },
    devtool: 'source-map'
}
