'use strict';

const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const $ = gulpLoadPlugins();
const merge = require('merge-stream');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminSvgo = require('imagemin-svgo');
const through2 = require('through2');
const cleanCSS = require('gulp-clean-css');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require("./webpack.config");
const browserSync = require('browser-sync').create();
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');

const spritePage = [
    'common',
    'top'
];

function sprite(cb) {
    spritePage.map((target) => {
        let spriteData = gulp.src(`./assets/image/sprites/${target}/*.png`)
            .pipe($.spritesmith({
                imgName: `${target}-sprites.png`,
                cssName: `_${target}-sprites.scss`,
                imgPath: `../../assets/img/${target}-sprites.png`,
                retinaSrcFilter: `./assets/image/sprites/${target}/*@2x.png`,
                retinaImgName: `${target}-sprites@2x.png`,
                retinaImgPath: `../../assets/img/${target}-sprites@2x.png`,
                //cssFormat: 'scss',
                padding: 20,
                cssVarMap: (sprite) => {
                    sprite.name = 'sprite-' + sprite.name;
                }
            }))
        let imgStream = spriteData.img.pipe(gulp.dest('assets/image/'));
        let cssStream = spriteData.css.pipe(gulp.dest('assets/scss/foundation/'));
        return merge(imgStream, cssStream);
    })
    cb();
};

function images() {
    return gulp
        .src([
            './assets/image/**/*.+(jpg|jpeg|png|gif|svg|ico)',
            '!./assets/image/sprites/*(common|top)/*'
        ])
        .pipe($.changed('./assets/img/'))
        .pipe($.imagemin([
            imageminMozjpeg({
                quality: 80
            }),
            imageminPngquant({
                quality: [.65, .80],
                speed: 1
            }),
            imageminGifsicle(),
            imageminSvgo()
        ], {
            verbose: true
        }))
        .pipe(gulp.dest('./assets/img/'))
};

function sass() {
    return gulp
        .src('./assets/scss/main.scss', {
            sourcemaps: true
        })
        .pipe($.plumber({
            errorHandler: $.notify.onError('<%= error.message %>')
        }))
        .pipe(autoprefixer({
            cascade: false,
            add: true,
        }))
        .pipe($.sass({
            includePaths: ['./node_modules']
        }))
        .pipe($.pleeease({
            autoprefixer: {
                "browsers": ["> 1%", "last 2 versions", "Firefox ESR", "Opera 12.1"]
            },
            minifier: false,
            mqpacker: true
        }))
        .pipe(through2.obj((file, enc, cb)=>{
            const date = new Date();
            file.stat.atime = date;
            file.stat.mtime = date;
            cb(null, file);
        }))
        .pipe(gulp.dest('./assets/css/', {
            sourcemaps: true
        }))

};

function cssMin() {
    return gulp
        .src(['./assets/css/*.css', '!./assets/css/*.min.css'])
        .pipe($.plumber({
            errorHandler: $.notify.onError('<%= error.message %>')
        }))
        .pipe($.sass())
        .pipe(cleanCSS())
        .pipe($.rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./assets/css/'));
};

function jsBundle() {
    return webpackStream(webpackConfig, webpack)
        .pipe(gulp.dest('./assets/js/'));
};

function serveReload(cb) {
    browserSync.reload();
    cb();
};

function serve() {
    browserSync.init({
        //必要に応じて
        //proxy: '192.168.33.10',
        //server: {baseDir: "./"},
        notify: false,
        logPrefix: 'serve'
    });
    gulp.watch('assets/image/sprites/**/*.png', sprite);
    gulp.watch(['assets/image/**/*.+(jpg|jpeg|png|gif|svg)', '!assets/image/sprites/**/*.png'], gulp.series('images', 'serveReload'));
    gulp.watch('assets/scss/**/*.scss', gulp.series('sass', 'cssMin', 'serveReload'));
    gulp.watch(['assets/js/**/*.js', '!assets/js/bundle.js', '!assets/js/main.js'], gulp.series('jsBundle', 'serveReload'));
    gulp.watch(['../../../app/template/**/*'], serveReload);
};

function clean(cb) {
    del(['dest/app','dest/html']);
    cb();
};

function copy() {
    let copyApp = gulp.src([
        '../../../app/template/mdc/**/*',
        '!../../../app/template/mdc/**/._*'
    ])
        .pipe($.changed('dest/app'))
        .pipe(gulp.dest('dest/app'))
    let copyHtml = gulp.src([
        './**/*',
        '!./**/._*',
        '!./package-lock.json',
        '!./node_modules',
        '!./node_modules/**',
        '!./assets/image',
        '!./assets/image/**',
        '!./dest',
        '!./dest/**'
    ], { dot: true })
        .pipe($.changed('dest/html'))
        .pipe(gulp.dest('dest/html'))
    return merge(copyApp, copyHtml);
};

exports.sprite = sprite;
exports.images = images;
exports.sass = sass;
exports.cssMin = cssMin;
exports.jsBundle = jsBundle;
exports.serveReload = serveReload;
exports.serve = serve;
exports.clean = clean;
exports.copy = copy;
