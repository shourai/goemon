# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Goemon's EC Cube v4 web site repository : https://goemon.ec-cube.shop/
Hosting : EC-CUBE.CO Cloud solution, https://www.ec-cube.co/
ECCUBE v4.0.3

This repository is meant for the template and plugin updates done to the web shop system.

There is **NO backup of the DB** yet, so be **extra careful** with saving changes outside of the plugin and templates!

Updates of the CSS is done in **default/html/user_data/assets/css/customize.css**
Updates of the JS is done in **default/html/user_data/assets/js/customize.js**

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact